<?php

namespace Drupal\formajax;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;

/**
 * Helper to add ajax dependency easily in forms.
 */
class AjaxDependency {

  /**
   * Converts a form element in the add view wizard to be AJAX-enabled.
   *
   * This function takes a form element and adds AJAX behaviors to it such that
   * changing it triggers another part of the form to update automatically. It
   * also adds a submit button to the form that appears next to the triggering
   * element and that duplicates its functionality for users who do not have
   * JavaScript enabled (the button is automatically hidden for users who do
   * have JavaScript).
   *
   * To use this function, call it directly from your form builder function
   * immediately after you have defined the form element that will serve as the
   * JavaScript trigger. Calling it elsewhere (such as in hook_form_alter()) may
   * mean that the non-JavaScript fallback button does not appear in the correct
   * place in the form.
   *
   * @param array $trigger_parent
   *   The element whose child will server as the AJAX trigger. For example, if
   *   $form['some_wrapper']['triggering_element'] represents the element which
   *   will trigger the AJAX behavior, you would pass $form['some_wrapper'] for
   *   this parameter.
   * @param string $trigger_key
   *   The key within the wrapping element that identifies which of its children
   *   serves as the AJAX trigger. In the above example, you would pass
   *   'triggering_element' for this parameter.
   * @param array $form_target_parents
   *   An array of parent keys that point to the part of the form that will be
   *   refreshed by AJAX. For example, if triggering the AJAX behavior should
   *   cause $form['dynamic_content']['section'] to be refreshed, you would pass
   *   array('dynamic_content', 'section') for this parameter. Note that this is
   *   relative to the whole form, not $trigger_parent.
   * @param string $stable_unique_prefix
   *   A prefix for the wrapper id that is
   *   - suitable for html IDs (use letters, numbers and '-'
   *   - unique on the page
   *   - stable from request to request
   *   As we only see the wrapping element here and not the complete form, this
   *   method needs external knowledge for creating a stable unique ID.
   *   As $form_target_parents are appendend, the prefix can be reused for
   *   different dependent targets in the same form.
   */
  public function add(array &$trigger_parent, $trigger_key, array $form_target_parents, $stable_unique_prefix) {
    $seen_buttons = &drupal_static(__FUNCTION__ . ':seen_buttons', []);

    // Add the AJAX behavior to the triggering element.
    $triggering_element = &$trigger_parent[$trigger_key];
    $triggering_element['#ajax']['callback'] = static::class . '::updateForm';

    // We do not use \Drupal\Component\Utility\Html::getUniqueId() to get an ID
    // for the AJAX wrapper, because it remembers IDs across AJAX requests (and
    // won't reuse them), but in our case we need to use the same ID from
    // request to request so that the wrapper can be recognized by the AJAX
    // system and its content can be dynamically updated. So instead, we need
    // prepend $stable_unique_prefix here.
    $refresh_parents_imploded = implode('-', $form_target_parents);
    $triggering_element['#ajax']['wrapper'] = "edit-form-ajax-dependency-$stable_unique_prefix-$refresh_parents_imploded-wrapper";

    // Add a submit button for users who do not have JavaScript enabled. It
    // should be displayed next to the triggering element on the form.
    $button_key = $trigger_key . '_trigger_update';
    $element_info = \Drupal::service('element_info');
    $trigger_parent[$button_key] = [
      '#type' => 'submit',
      // Hide this button when JavaScript is enabled.
      '#attributes' => ['class' => ['js-hide']],
      '#submit' => [static::class . '::nojsSubmit'],
      // Add a process function to limit this button's validation errors to the
      // triggering element only. We have to do this in #process since until the
      // form API has added the #parents property to the triggering element for
      // us, we don't have any (easy) way to find out where its submitted values
      // will eventually appear in $form_state->getValues().
      '#process' => array_merge([static::class . '::limitValidation'], $element_info->getInfoProperty('submit', '#process', [])),
      // Add an after-build function that inserts a wrapper around the region of
      // the form that needs to be refreshed by AJAX (so that the AJAX system
      // can detect and dynamically update it). This is done in #after_build
      // because it's a convenient place where we have automatic access to the
      // complete form array, but also to minimize the chance that the HTML we
      // add will get clobbered by code that runs after we have added it.
      '#after_build' => array_merge($element_info->getInfoProperty('submit', '#after_build', []), ['ajaxeasy_add_ajax_wrapper']),
    ];
    // Copy #weight and #access from the triggering element to the button, so
    // that the two elements will be displayed together.
    foreach (['#weight', '#access'] as $property) {
      if (isset($triggering_element[$property])) {
        $trigger_parent[$button_key][$property] = $triggering_element[$property];
      }
    }
    // For easiest integration with the form API and the testing framework, we
    // always give the button a unique #value, rather than playing around with
    // #name. We also cast the #title to string as we will use it as an array
    // key and it may be a TranslatableMarkup.
    $button_title = !empty($triggering_element['#title']) ? (string) $triggering_element['#title'] : $trigger_key;
    if (empty($seen_buttons[$button_title])) {
      $trigger_parent[$button_key]['#value'] = t('Update "@title" choice', [
        '@title' => $button_title,
      ]);
      $seen_buttons[$button_title] = 1;
    }
    else {
      $trigger_parent[$button_key]['#value'] = t('Update "@title" choice (@number)', [
        '@title' => $button_title,
        '@number' => ++$seen_buttons[$button_title],
      ]);
    }

    // Attach custom data to the triggering element and submit button, so we can
    // use it in both the process function and AJAX callback.
    $ajax_data = [
      'wrapper' => $triggering_element['#ajax']['wrapper'],
      'trigger_key' => $trigger_key,
      'form_target_parents' => $form_target_parents,
    ];
    $triggering_element['#form_ajax_dependency_ajax_data'] = $ajax_data;
    $trigger_parent[$button_key]['#form_ajax_dependency_ajax_data'] = $ajax_data;
  }

  /**
   * Processes a non-JS fallback submit button to limit its validation errors.
   *
   * @param array $element
   *   The element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The processed element.
   */
  public function limitValidation(array $element, FormStateInterface $form_state) {
    // Retrieve the AJAX triggering element so we can determine its parents. (We
    // know it's at the same level of the complete form array as the submit
    // button, so all we have to do to find it is swap out the submit button's
    // last array parent.)
    $array_parents = $element['#array_parents'];
    array_pop($array_parents);
    $array_parents[] = $element['#form_ajax_dependency_ajax_data']['trigger_key'];
    $ajax_triggering_element = NestedArray::getValue($form_state->getCompleteForm(), $array_parents);

    // Limit this button's validation to the AJAX triggering element, so it can
    // update the form for that change without requiring that the rest of the
    // form be filled out properly yet.
    $element['#limit_validation_errors'] = [$ajax_triggering_element['#parents']];

    // If we are in the process of a form submission and this is the button that
    // was clicked, the form API workflow in
    // \Drupal::formBuilder()->doBuildForm() will have already copied it to
    // $form_state->getTriggeringElement() before our #process function is run.
    // So we need to make the same modifications in $form_state as we did to the
    // element itself, to ensure that #limit_validation_errors will actually be
    // set in the correct place.
    $clicked_button = &$form_state->getTriggeringElement();
    if ($clicked_button && $clicked_button['#name'] == $element['#name'] && $clicked_button['#value'] == $element['#value']) {
      $clicked_button['#limit_validation_errors'] = $element['#limit_validation_errors'];
    }

    return $element;
  }

  /**
   * After-build function that adds a ajax wrapper to a form region.
   *
   * This function inserts a wrapper around the region of the form that needs to
   * be refreshed by AJAX, based on information stored in the corresponding
   * submit button form element.
   *
   * @param array $element
   *   The element to add to.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The modified element.
   */
  public function addWrapper(array $element, FormStateInterface $form_state) {
    // Find the region of the complete form that needs to be refreshed by AJAX.
    // This was earlier stored in a property on the element.
    $complete_form = &$form_state->getCompleteForm();
    $refresh_parents = $element['#form_ajax_dependency_ajax_data']['refresh_parents'];
    $refresh_element = NestedArray::getValue($complete_form, $refresh_parents);

    // The HTML ID that AJAX expects was also stored in a property on the
    // element, so use that information to insert the wrapper <div> here.
    $id = $element['#form_ajax_dependency_ajax_data']['wrapper'];
    $refresh_element += [
      '#prefix' => '',
      '#suffix' => '',
    ];
    $refresh_element['#prefix'] = '<div id="' . $id . '" class="form-ajax-dependency-ajax-wrapper">' . $refresh_element['#prefix'];
    $refresh_element['#suffix'] .= '</div>';

    // Copy the element that needs to be refreshed back into the form, with our
    // modifications to it.
    NestedArray::setValue($complete_form, $refresh_parents, $refresh_element);

    return $element;
  }

  /**
   * Updates a part of the add view form via AJAX.
   *
   * @return array
   *   The part of the form that has changed.
   */
  public function updateForm($form, FormStateInterface $form_state) {
    // The region that needs to be updated was stored in a property of the
    // triggering element by ::add(), so all we have to do is
    // retrieve that here.
    return NestedArray::getValue($form, $form_state->getTriggeringElement()['#form_ajax_dependency_ajax_data']['refresh_parents']);
  }

  /**
   * Non-Javascript fallback for updating the add view form.
   */
  public function nojsSubmit($form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

}
